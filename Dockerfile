FROM node:current-alpine
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
RUN npx @11ty/eleventy

FROM nginx
COPY ./default.conf.template /etc/nginx/conf.d/default.conf.template
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./_site/index.html /usr/share/nginx/html
COPY ./assets /usr/share/nginx/html
CMD /bin/bash -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'
